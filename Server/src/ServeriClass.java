import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;

public class ServeriClass {


    public static void main(String[] args) throws IOException {
        startServer();
    }

    public static void startServer() throws IOException {

        ServerSocket serverSocket = new ServerSocket(8082);
        while (true) {
            try (Socket clientConnection = serverSocket.accept()) {
                handleClientConnection(clientConnection);
            }
        }
    }

    private static void handleClientConnection(Socket clientConnection) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(clientConnection.getInputStream()));
        StringBuilder requestBuilder = new StringBuilder();
        String line;
        while (!(line = br.readLine()).isBlank()) {
            requestBuilder.append(line + "\n");
        }
        String request = requestBuilder.toString();
        System.out.println(request);
        String[] requestLines = request.split("\n\n");
        String[] requestLine = requestLines[0].split(" ");
        String longFileName = requestLine[1];
        String fileName = longFileName.substring(1);
        String contentType = Files.probeContentType(Path.of(fileName));
        if (Files.exists(Path.of(fileName))) {
            sendResponse(clientConnection, "200 OK", contentType, Files.readAllBytes(Path.of(fileName)));
        } else {
            byte[] notFoundContent = "<h1>Not found :(</h1>".getBytes();
            sendResponse(clientConnection, "404 Not Found", "text/html", notFoundContent);
        }
    }

    public static void sendResponse(Socket clientConnection, String status, String contentType, byte[] content) throws IOException {
        OutputStream clientOutput = clientConnection.getOutputStream();
        clientOutput.write(("HTTP/1.1 " + status + "\n").getBytes());
        clientOutput.write(("ContentType: " + contentType + "\n").getBytes());
        clientOutput.write("\n".getBytes());
        clientOutput.write(content);
        clientOutput.flush();
        clientConnection.close();
    }

}



