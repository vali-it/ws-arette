import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class HttpServer {

    public static void main(String[] args) throws IOException {
        connectToServer();
    }

    public static void connectToServer() throws IOException {
        ServerSocket serverSocket = new ServerSocket(8081);
        System.out.println("smth");
        while(true) {
            try(Socket connectSocket = serverSocket.accept()) {
                String answer = "HTTP/1.1 200 OK \n\nHello World";
                connectSocket.getOutputStream().write(answer.getBytes("UTF-8"));
            }
        }
    }
}