package framework;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class BusinessLogicService {

    public static void main(String[] args) throws IOException {
        startServer();
    }

    public static void startServer() throws IOException {
        ServerSocket serverSocket = new ServerSocket(8080);
        while (true) {
            try (Socket clientConnection = serverSocket.accept()) {
                handleClientConnection(clientConnection);
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    private static void handleClientConnection(Socket clientConnection) throws IOException, InvocationTargetException, IllegalAccessException {
        BufferedReader br = new BufferedReader(new InputStreamReader(clientConnection.getInputStream()));
        StringBuilder requestBuilder = new StringBuilder();
        String line;
        while (!(line = br.readLine()).isBlank()) {
            requestBuilder.append(line + "\n");
        }
        String request = requestBuilder.toString();
        String[] requestLines = request.split("\n\n");
        String[] requestLine = requestLines[0].split(" ");
        String requestPath = requestLine[1];
        System.out.println(requestPath+"yyyy");
        responseBodyString(clientConnection, requestPath);
    }

    public static Map<String, Method> getMethods() {
        Map<String, Method> methodMap = new HashMap();
        for (Method method : BusinessLogicController.class.getDeclaredMethods()) {
            WebPath annotation = method.getAnnotation(WebPath.class);
            methodMap.put(annotation.path(), method);
        }
        return methodMap;
    }

    public static void responseBodyString(Socket clientConnection, String requestPath) throws InvocationTargetException, IllegalAccessException, IOException {
        Map<String, Method> methodMap = getMethods();

        if(!methodMap.containsKey(requestPath)) {
//            byte[] notFoundContent = "<h1>Not found :(</h1>".getBytes();
//            errorResponse(clientConnection, "404 Not Found", "text/html", notFoundContent);
            sendResponse(clientConnection, errorResponse());
        } else {
            Object invoke = methodMap.get(requestPath).invoke(new BusinessLogicController(),"", requestPath, Map.of());
            byte[] response = (byte[]) invoke;
//            String methodPrint = new String (response);
//            System.out.println(methodPrint);
            sendResponse(clientConnection, response);
        }
    }

    public static void sendResponse(Socket clientConnection, byte[] response) throws IOException {
        OutputStream clientOutput = clientConnection.getOutputStream();
        clientOutput.write(response);
    }

    public static void errorVastus(Socket clientConnection, String status, String contentType, byte[] content) throws IOException {
        OutputStream clientOutput = clientConnection.getOutputStream();
        clientOutput.write(("HTTP/1.1 " + status + "\n").getBytes());
        clientOutput.write(("ContentType: " + contentType + "\n").getBytes());
        clientOutput.write("\n".getBytes());
        clientOutput.write(content);
        clientOutput.flush();
        clientConnection.close();
    }

    public static byte[] errorResponse() {
        StringBuilder error = new StringBuilder();
        error.append("HTTP/1.1 404 Not Found \n");
        error.append("ContentType: text/html\n");
        error.append("\n\n");
        error.append("404 Not Found");

        return error.toString().getBytes(StandardCharsets.UTF_8);
    }

}



