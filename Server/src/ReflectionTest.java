import framework.BusinessLogicController;
import framework.WebPath;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class ReflectionTest {
    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException {
        getMethods();
    }

    public static Map<String, Method> getMethods() throws InvocationTargetException, IllegalAccessException {
        Map<String, Method> methodMap = new HashMap();
        for (Method method : BusinessLogicController.class.getDeclaredMethods()) {
            WebPath annotation = method.getAnnotation(WebPath.class);
            System.out.println(annotation.path());
            methodMap.put(annotation.path(), method);
        }
        return methodMap;
    }
//    public static String responseBodyString() throws InvocationTargetException, IllegalAccessException{
//        Map<String, Method> methodMap = getMethods();
////        String methodKey = Server3.getDesiredMethodName();
//        Object invoke = methodMap.get("/hello").invoke(
//                new BusinessLogicController(),
//                "",
//                "/hello",
//                Map.of());
//        byte[] bait = (byte[]) invoke;
//        String baitString = new String(bait);
//        return baitString;
//    }
}